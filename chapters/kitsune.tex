\section{Kitsune NIDS}
Come abbiamo gi\`a detto, Kitsune \`e un NIDS basato su reti neurali,
usate per rilevare \textit{pattern} anomali nel traffico della rete.
L'idea \`e quella di rilevare le anomalie mediante un insieme di
\textit{autoencoder} (quelli del primo livello), ognuno responsabile di uno specifico aspetto del
comportamento della rete.

\subsection{Struttura}
Kitsune \`e composto dai componenti di seguito elecati:
\begin{itemize}
    \item \textbf{Catturatore di Pacchetti}: libreria esterna
          responsabile per l'acquisizione dei pacchetti.
    \item \textbf{Parser di Pacchetti}: libreria esterna responsabile
          del \textit{parsing} dei metadati dei pacchetti.
    \item \textbf{Estrattore di Caratteristiche (FE)}: componente
          responsabile dell'estrazione di $n$ caratteristiche dal pacchetto
          in arrivo per la creazione dell'istanza $\vec{x}\in\mathbb{R}^n$.
    \item \textbf{Mappatore di caratteristiche (FM)}: componente
          responsabile per la creazione di un set di istanze pi\`u piccole (che
          chiameremo \textbf{v}) da $\vec{x}$ e per l'apprendimento della
          mappatura da $\vec{x}$ a \textbf{v}.
    \item \textbf{Rilevatore di Anomalie (AD)}: componente responsabile
          il rilevamento di pacchetti anomali, data la rappresentazione
          \textbf{v} del pacchetto.
\end{itemize}

L'unico parametro che KitNET riceve \`e il massino numero di input $m$
che ogni \textit{autoencoder} pu\`o ricevere. Tale parametro influisce
sulla complesist\`a. Lasciamo quindi all'utente la scelta tra performance
in termini di tempo di esecuzione e performance in termini di rilevamenti.

Vogliamo adesso specificare che FM e AD possono operare in due modi diversi:
in modalit\`a di addestramento le variabili interne vengono
cambiate senza che venga prodotto un output, e in modalit\`a di esecuzione
nessuna variabile interna viene variata ma viene prodotto un output.

Prima di passare ad un'analisi pi\`u dettagliata del funzionamento di
Kitsune, vogliamo descrivere cosa accade quando un pacchetto viene
recevuto:
\begin{enumerate}
    \item il \textbf{Catturatore di Pacchetti} acquisisce il pacchetto grezzo e
          lo passa al Parser di Pacchetti.
    \item il \textbf{Parser di Pacchetti} analizza il pacchetto grezzo
          estraendo le informazioni per il FE.
    \item il \textbf{FE} ricava dai dati ricevuti pi\`u di 100 statistiche
          che sono usate per descrivere lo stato del canale dal quale \`e stato
          ricevuto il pacchetto. Tali statistiche formano l'istanza
          $\vec{x}\in\mathbb{R}^n$ che viene inoltrata al FM.
    \item il \textbf{FM} riceve $\vec{x}$\dots
          \begin{itemize}
              \item \textbf{Modalit\`a Addestramento}: \dots e lo usa per
                    apprendere la mappatura delle caratteristiche. Tale mappatura
                    raggruppa le caratteristiche di $\vec{x}$ in modo tale che ognuna
                    di queste abbia dimensione massima $m$. Niente viene passato all' AD
                    fino a che la mappatura non \`e stata completata. Alla fine della
                    fase di addestramento la mappatura viene inviata all'AD che la usa
                    per generare l'architettura del primo livello di \textit{autoencoder}.
              \item \textbf{Modalit\`a Esecuzione}: \dots e, tramite la mappatura
                    precedentemente generata, crea un insieme di piccole istanze
                    \textbf{v} a partire da $\vec{x}$, ognuna delle quali \`e poi
                    inoltrata al rispettivo \textit{autoencoder} del primo livello 
                    del AD.
          \end{itemize}
    \item l' \textbf{AD} riceve \textbf{v}\dots
          \begin{itemize}
              \item \textbf{Modalit\`a Addestramento}: \dots e lo usa per
                    addestrare il primo livello di \textit{autoencoder}. 
                    Lo SQM della \textit{forward-propagation} \`e poi usato per 
                    addestrare il \textit{layer} di output (il secondo livello). 
                    Lo SQM maggiore del \textit{layer} di output \`e salvato in 
                    $\phi$ (il valore soglia per il rilevamento)
                    per essere utilizzato in seguito.
              \item \textbf{Modalit\`a Esecuzione}: \dots analizza \textbf{v}.
                    Se lo SQM dell'output supera $\phi\beta$ (con $\beta\in[1,\infty)$
                    parametro di sensibilit\`a), allora un'allerta viene generata.
          \end{itemize}
    \item Il pacchetto originale $\vec{x}$ e \textbf{v} vengono scartati.
\end{enumerate}

\subsection{Estrattore di Caratteristiche (FE)}
Lo scopo di questa componente \`e quello di estrarre le caratteristiche
che identificano il contesto del pacchetto cha attraversa la rete.
Le sfide che presenta tale compito sono: la gestione di pacchetti da
canali diversi, la gestione di molteplici canali contemporaneamente e
un'elevata frequenza di arrivo dei pacchetti.

Per risolvere in modo efficace ed efficente i problemi appena presentati,
piuttosto che una soluzione a finestra scorrevole, abbiamo optato per
l'uso di statistiche incrementali con una finestra di decadimento.
Ci\`o significa che le caratteristiche estratte sono le pi\`u recenti
e che la statistica incrementale viene eliminata quando il suo peso
arriva a zero (allo scadere del tempo della finestra di decadimento).

Mostreremo ora nel dettaglio il procedimento precedentemente accennato
per poi specificare le caratteristiche estratte dal modulo in analisi.

\subsubsection{Statistiche Incrementali con Decadimento}
Sia $S=\{x_1,\dots\}$ uno \textit{stream} di dati con $x_i \in \mathbb{R}$.
La media, la varianza e la deviazione standard possono essere aggiornate
incrementalmente mantenendo la tupla $IS:=(N,LS,SS)$, con $N$, $LS$ e $SS$
rispettivamente numero, somma lineare e somma dei quadrati delle istanze.
Quindi la procedura per inserire $x_i$ in $IS$ \`e $IS\leftarrow(N+1,
    LS+x_i,SS+x_i^2)$.In ogni momento le statistiche sono $\mu_S=\frac{LS}{N}$,
$\sigma_S^2=|\frac{SS}{N}-\mu_S^2|$ e $\sigma_S=\sqrt{\sigma_S^2}$.

Nel modello a finestra di decadimento il peso dei valori precedenti decresce
esponenzialmente con il passare del tempo. La funzione di decadimento \`e
definita come $d_\lambda(t)=2^{-\lambda t}$, con $\lambda>0$ fattore di
decadimento e $t$ il tempo trascorso dall'ultima osservazione dallo
\textit{stream} $S_i$.

La tupla di una statistica incrementale con decadimento \`e quindi definita
come $IS_{i,\lambda}:=(w,LS,SS,SR_{ij},T_{last})$, con $w$ peso attualmente
utilizzato, $T_{last}$ \textit{timestamp} dell'ultimo aggiornamento di
$IS_{i,\lambda}$ e $SR_{ij}$ somma dei prodotti residui tra gli stream $i$
e $j$ (usato per statistiche in 2D, come relazioni tra traffico in entrata
e in uscita).

In questo caso l'aggiornamento del vettore diventa:

\begin{algorithmic}
    \Procedure{update}{$IS_{i,\lambda},x_{cur},t_{cur},r_j $} 
        \State $\gamma\gets d_\lambda(t_{cur}-t_{last})$
            \Comment{Calcolo fattore di decadimento}
        \State $IS_{i,\lambda}\gets(\gamma w, \gamma LS, \gamma SS,
        \gamma SR, T_{cur})$
            \Comment{applico processo di decadimento}
        \State $IS_{i,\lambda}\gets(w+1,LS+x_{cur},SS+x_i^2,
        SR_{ij}+r_ir_j, T_{cur})$
            \Comment{Inserimento}
        \State return $IS_{i,\lambda}$
    \EndProcedure
\end{algorithmic}

\subsubsection{Caratteristiche estratte}
All'arrivo di un pacchetto viene estratta un'istantanea del comportamento 
del \textit{host} e dei protocolli utilizzati per comunicare il pacchetto.
Tale istantanea \`e formata da 115 statistiche che in un breve intervallo 
identificano il mittente del pacchetto e il traffico tra mittente e 
destinatario.

Tutto il traffico pu\`o essere riassunto da:
\begin{itemize}
    \item \textbf{SrcMAC-IP}: traffico originato dal MAC e dall' IP 
    di provenienza del pacchetto.
    \item \textbf{SrcIP}: traffico originato dall'IP di provenienza 
    del pacchetto.
    \item \textbf{Channel}: tarffico tra l'IP del mittente e quello del 
    destinatario del pacchetto.
    \item \textbf{Socket}: traffico tra la \textit{socket} TCP/UDP 
    del mittente del pacchetto e quella del destinatario.
\end{itemize}

Da queste possono essere estratte 23 caratteristiche da una singola 
finestra di tempo. Tali caratteristiche vengono estratte in 5 
diverse finestre di tempo (100ms, 500ms, 1.5sec, 10sec, 1min), per un 
totale di 115 caratteristiche.

Siamo quindi arrivati ad avere il vettore $\vec{x}\in\mathbb{R}^n$, con 
$n=115$ che passa dal FE al FM. 

\subsection{Mappatore di Caratteristiche (FM)}
Lo scopo di questo componente \`e quello di mappare le $n$ caratteristiche 
di $\vec{x}$ in $k$ instanze pi\`u piccole, una per ogni 
\textit{autoencoder} del primo livello.
Chiameremo l'insieme ordinato di k istanze 
$\textbf{v}=\{\vec{v_1},\dots,\vec{v_k}\}$.

Per avere la certezza che il primo livello dell' AD funzioni a dovere e con 
una complessit\`a adeguata abbiamo bisogno che la mappatura $f(\vec{x})=
\textbf{v}$:
\begin{itemize}
    \item Garantisca che ogni $\vec{v_i}$ abbia al massimo $m$ caratteristiche, 
    con $m$ parametro definito dall'utente.
    \item Mappi ognuna delle $n$ caratteristiche di $\vec{x}$ in esattamente 
    una caratteristica in \textbf{v}.
    \item Contenga sottospazi di $X$ (con $X$ dominio di $\vec{x}$) che 
    identifichino il comportamento normale abbastanza bene da poter rilevare 
    eventi anomali nei rispettivi sottospazi.
    \item Sia scoperto in un procedimento "in diretta" in modo tale che in memoria 
    non ve ne siano mai due salvati contemporaneamente.
\end{itemize}

Al fine di rispettare i requisiti appena elencati dobbiamo trovare una 
mappatura $f$ raggruppando, incrementalmente, le caratteristiche di $X$ 
in $k$ gruppi non pi\`u grandi di $m$.
In particolare l'algoritmo di mappatura effettua la seguenti azioni:
\begin{enumerate}
    \item In modalit\`a di addestramento aggiorna incrementalmente il vettore 
    delle caratteristiche di $\vec{x}$. 
    \item Quando la modalit\`a di addestramento finisce, un raggruppamento 
    gerarchico viene applicato per trovare $f$.
    \item In modalit\`a di esecuzione, $f(\vec{x}_t)=\textbf{v}$ viene 
    calcolato ed il risultato viene inoltrato all'AD.
\end{enumerate}

Per avere una maggiore sicurezza che le caratteristiche raggruppate 
catturino il comportamento normale, durante il procedimento di raggruppamento, 
utiliziamo la correlazione come la distanza tra due dimensioni.
In generale la distanza di correlazione tra due vettori \`e  definita 
$d_{cor}(u,v)=1-\frac{(u-\bar{u})\cdot(v-\bar{v})}
{||(u-\bar{u})||_2\cdot||(v-\bar{v})||_2}$, 
con $\bar{u}$ media degli elementi del vettore $u$ e $u \cdot v$ prodotto scalare.

Spiegheremo adesso come la distanza di correlazione tra le caratteristiche 
pu\`o essere calcolata incrementalmente al fine di raggruppare le caratteristiche.
Sia $n_t$ il numero delle instanze trovate. 
Sia $\vec{c}$ un vettore a $n$ dimensioni contenente la somma lineare di 
ogni caratteristica, tale che l'elemento $c^{(i)}=\sum_{t=0}^{n_t}x_t^{(i)}$, 
dove $i$ indica la caratteristica e $t$ il tempo.
Sia adesso $\vec{c_r}$ un vettore contenente la somma dei residui di ogni 
caratteristica tale che $c_r^{(i)}=\sum_{t=o}^{n_t}(x_t^{(i)}-
\frac{c^{(i)}}{n_t})$ e  $\vec{c_{rs}}$ un vettore contenente la somma 
del quadrato dei residui di ogni caratteristica tale che $c_{rs}^{(i)}=\sum_{t=o}^{n_t}(x_t^{(i)}-
\frac{c^{(i)}}{n_t})^2$.
Sia $C$ la matrice di correlazione parziale $n \times n$ dove 
$[C^{i,j}]=\sum_{t=0}^{n_t}((x_t^{(i)}-\frac{c^{(i)}}{n_t})(x_t^{(j)}-
\frac{c^{(j)}}{n_t}))$ \`e la sommma di prodotti tra i residui della 
caratteristica $i$ e $j$.
Sia infine $D$ la matrice di distanza di correlazione tra ogni caratteristica 
di $X$.

La matrice $D$ pu\`o essere calcolata in ogni momento, usando $C$ e 
$\vec{c_{rs}}$, come 
$D=[D_{ij}]=1-\frac{C_{i,j}}{\sqrt{c_{rs}^{(i)}}\sqrt{c_{rs}^{(j)}}}$.

Ora che \`e chiaro come possiamo ottenere la matrice delle distanze in modo 
incrementale, possiamo effettuare un raggruppamento gerarchico su $D$ al fine 
di trovare $f$. L'algoritmo inizia con n gruppi, uno per ogni punto di $D$, 
cerca i due punti pi\`u vicini e unisce i relativi gruppi. Tale procedura di 
"cerca e unisci" si ripete fino a che non sia arriva ad avere un solo cluster 
contenente tutt gli $n$ punti. 

L'albero che descrive tale procedura, detto 
dendrogramma, \`e usato per ottenere $k$ gruppi non pi\`u grandi di $m$.
Per arrivare a tale obiettivo il dendrogramma viene scisso a partire dalla 
radice per poi controllare che la cardinalit\`a dei gruppi risultanti sia 
minore o uguale a $m$. Tale procedura viene ripetuta ricorsivamente sui 
gruppi con dimensione maggiore di $m$. 
Quando tale procedimento si conclude, il risultato sono $k$ gruppi di 
caratteristiche con una forte correlazione interna, dove nessun gruppo 
\`e pi\`u grande di $m$. 
Tale raggruppamenti sono poi salvati e usati per effettare la mappatura $f$. 

\subsection{Rilevatore di Anomalie (AD)}
Come precedentemente anticipato, questa componente contiene una speciale 
rete neurale, che abbiamo chiamato KitNET, che \`e una ANN non supervisionata 
progettata per il rilevamento di anomalie. 
KitNET \`e composta da due livelli:
{\renewcommand{\labelenumi}{\Roman{enumi}}
\begin{enumerate}
      \item \textbf{livello} o livello legione:
      
      insieme ordinato di $k$ \textit{autoencoder} a tre strati, ognuno mappato 
      alla relativa istanza in \textbf{v}.
      Questo livello si occupa della misurazione della variazione dal normale 
      di ogni sottospazio di \textbf{v}.
      Durate la fase di addestramento, gli \textit{autoencoder} apprendono il 
      comportamento normale del rispettivo sottospazio.
      In entrambe le fasi, di addestramento e di esecuzione, ogni autoencoder 
      inoltra lo SQM dovuto all'errore commesso nella ricostruzione al 
      livello successivo.

      \item \textbf{livello} o livello di \textit{output}:
      
      \textit{autoencoder} a tre strati che apprende gli SQM normali del primo 
      livello. 
      Questo livello \`e responsabile del verdetto finale, prodotto sotto forma 
      di un punteggio di anomalia che considera la relazioni tra le anormalit\`a 
      dei sottospazi e il rumore che \`e normalmente rilevato nel traffico di rete. 
\end{enumerate}}

Vedremo ora nel dettaglio il funzionamento della componente in analisi.

\subsubsection{Inizializzazione}
Quando l'AD riceve il primo set di istanze \textbf{v} dal FM, l'AD inizializza 
l'architettura di KitNET basandosi su \textbf{v}.
In particolare, sia $\theta$ un \textit{autoencoder} e siano 
$L^{(1)}$ e $L^{(2)}$ 
rispettivamente il primo e il secondo livello dell'AD.

$L^{(1)}$ \`e definito come l'insieme ordinato 
$L^{(1)}=\{\theta_1,\dots,\theta_k\}$ tale che \\ l'\textit{autoencoder} 
$\theta_i\in L^{(1)}$ ha tre strati di neuroni: 
$\textbf{dim}(\vec{v_i})$ in \textit{input} e \textit{output}, e 
$\lceil\beta\cdot\textbf{dim}(\vec{v_i})\rceil$ nello strato intermedio, con 
$\beta \in (0,1)$.

$L^{(2)}$ \`e definito come il singolo \textit{autoencoder} $\theta_0$, con $k$ 
neuroni sia in \textit{input} che in \textit{output} e 
$\lceil\beta\cdot k\rceil$ neuroni intermedi.

$L^{(1)}$ e $L^{(2)}$ non sono connessi mediante le "sinapsi con peso" come 
nelle ANN comuni. Invece gli input di $L^{(2)}$ sono gli SQM degli 
\textit{autoencoder} di $L^{(1)}$ normalizzati tra $0$ e $1$.
Infine, i pesi dell'\textit{autoencoder} $\theta_i$ sono inizializzati con 
valori randomici dalla distribuzione uniforme 
$U(\frac{-1}{\textbf{dim}(\vec{v_i})},\frac{1}{\textbf{dim}(\vec{v_i})})$.

\subsubsection{Modalit\`a di Addestramento}
L'addestramento di KitNET \`e diverso da quello di una comune ANN, questo 
poich\`e lo SQM dell'errore di ricostruzione passa tra i due livelli della 
rete neurale.
Inoltre KitNET \`e addestrato mediante l'utilizzo della 
discesa stocastica del gradiente, utilizzando ogni istanza osservata \textbf{v} 
esattamente una volta.

L'addestramento deve essere effettuato esclusivamente su dati normali, ovvero 
non devono essere presenti dati derivanti da eventuali attacchi.

\subsubsection{Modalit\`a di Esecuzione}
Durante la modalit\`a di esecuzione i parametri interni di KitNET non vengono 
aggiornati, invece viene effettuata la \textit{forward propagation} attraverso 
tutta la rete neurale, che ritorna lo SQM dell'errore di ricostruzione di $L^{(2)}$.

L'errore di ricostruzione di $L^{(2)}$ misura l'anormalit\`a dell'istanza, tenendo 
in considerazione le relazioni tra i sottospazi di \textbf{v}.
Ci\`o comporta che se due caratteristiche sono risultate correlate in modalit\`a 
di addestramento, una mancanza di tale correlazione in modalit\`a di esecuzione 
pu\`o essere considerata un'anomalia in modo pi\`u significativo rispetto alla sola 
somma degli SQM del primo livello di \textit{autoencoder}.

\subsubsection{Punteggio di Anomalia}
L'\textit{output} di KitNET \`e uno SQM (che funge da punteggio di anomalia) 
$s\in[0,\infty)$.
Maggiore \`e il valore di $s$, maggiore sar\`a l'anomalia dell'istanza in 
analisi.

Per usare $s$, \`e necessario prima definire il valore soglia $\phi$. 
Un approccio \textit{naive} sarebbe quello di assegnare a $\phi$ il valore di 
anomalia pi\`u grande trovato nella modalit\`a di addestramento.
Un altro approccio \`e quello di decidere il valore di $\phi$ in modo 
probabilistico, ovvero possiamo effettuare il \textit{fit} degli SQM 
con una distribuzione lognormale o con una distribuzione non-standard, 
per poi sollevare un'allerta se $s$ ha una probabilit\`a bassa di occorrenza.

Sar\`a quindi l'utente che dovr\`a scegliere il metodo migliore per trovare $\phi$ 
in funzione all'ambito in cui l'algoritmo operer\`a.
\newpage

\subsection{Complessit\`a}
Vogliamo ora confrontare la complessit\`a di KitNET con quella di un singolo 
\textit{autoencoder} a tre strati agente su $\vec{x}\in\mathbb{R}^n$, 
con uno strato di compressione con rapporto $\beta\in(0,1]$.

La complessit\`a derivante dall'esecuzione di un singolo autoencoder \`e \\
$O(n\cdot\beta n+\beta n \cdot n) = O(n^2)$, ci\`o deriva dal fatto che la 
complessit\`a derivante dall'attivazione dello strato $l^{(i+1)}$ di una ANN 
equivale a $O(l^{(i)}\cdot l^{(i+1)})$.

La complessit\`a di KitNET \`e $O(km^2+k^2)=O(k^2)$, dove $O(km^2)$ 
deriva dall'esecuzione di $L^{(1)}$ e $O(k^2)$ da quella di $L^{(2)}$.
In particolare $k$ indica il numero di \textit{autoencoder} (o di sottospazi) 
selezionati dal FM e $m$ \`e il parametro in input che indica il numero 
massimo di \textit{input} per ogni autoencoder appartenente a $L^{(1)}$.
Si noti che la complessit\`a risultante di KitNET diventa $O(k^2)$ poich\`e 
$m$ \`e costante durante l'esecuzione.

Il risultato appena mostrato indica che la complessit\`a del primo livello 
di \textit{autoencoder} scala linearmente con $n$, tuttavia la complessit\`a 
del secondo livello diende da quanti \textit{autoencoder} (o sottospazi) sono 
richiesti dal FM. 
Il caso ottimo \`e quando $k=\frac{n}{m}$ poich\`e le performance migliorano di un 
fattore $m$.
Il caso pessimo, invece, \`e quando il FM designa ogni singolo \textit{autoencoder} 
di $L^{(1)}$ ad una singola caratteristica. 
In questo caso $k=n$, e KitNET lavora 
come un singolo grande \textit{autoencoder}, senza che ci sia un miglioramento 
nelle performance.
Ci\`o accade anche se l'utente imposta $m=1$ o $m=n$.

Infine la complessit\`a  dell'addestramento di KitNET \`e $O(k^2)$ poich\`e 
utilizziamo ogni istanza solo una volta. Questo \`e in contrasto con i 
classificatori basati sulle ANN che tipicamente effettuano molteplici 
passaggi del dataset.
